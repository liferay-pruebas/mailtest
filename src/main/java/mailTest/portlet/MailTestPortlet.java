package mailTest.portlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Scanner;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.util.ContentUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletContext;

import mailTest.constants.MailTestPortletKeys;

import org.osgi.service.component.annotations.Component;

/**
 * @author dramirez
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=MailTest Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MailTestPortletKeys.MailTest,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class MailTestPortlet extends MVCPortlet {
	
	private final String from = "dsnotif@gmail.com";
	
	private final File attachment = new File(PropsUtil.get("liferay.home")+"/data/TortoiseSVN.pdf");
	private final String attachmentName = "TortoiseSVN.pdf"; 
	
	private final File plantilla = new File(PropsUtil.get("liferay.home")+"/data/miplantilla.tmpl");
	
	
	public void sendTextEmail(ActionRequest req, ActionResponse resp){
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		
		String to = req.getParameter("to");
		if(to == null || to.trim().equals("")){
			SessionErrors.add(req, "mailEmpty");
		}

		try {
	    		fromAddress = new InternetAddress(from);
	    		toAddress = new InternetAddress(to);
	    		MailMessage mailMessage = new MailMessage();
	    		mailMessage.setTo(toAddress);
	    		mailMessage.setFrom(fromAddress);
	    		mailMessage.setSubject("Ejemplo texto");
	    		mailMessage.setBody("Ejemplo de email con texto plano.");
	    		MailServiceUtil.sendEmail(mailMessage);
	    		System.out.println("Sending mail with Plain Text");
		} catch (AddressException e) {
		    	e.printStackTrace();
		}
	}
	
	public void sendTextEmailWithAttachment(ActionRequest req, ActionResponse resp){
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		
		String to = req.getParameter("to");
		if(to == null || to.trim().equals("")){
			SessionErrors.add(req, "mailEmpty");
		}
		
		//File file = new File("C:\\Users\\dramirez\\Desktop\\subidas\\2348171475474915mozilla12-pdf.pdf");
	    
		try {
			fromAddress = new InternetAddress(from);
    		toAddress = new InternetAddress(to);
			MailMessage mailMessage = new MailMessage();
			mailMessage.setTo(toAddress);
			mailMessage.setFrom(fromAddress);
			mailMessage.setSubject("Ejemplo con adjunto");
			mailMessage.setBody("Ejemplo de env�o con un fichero adjunto.");
			mailMessage.addFileAttachment(attachment, attachmentName);
			MailServiceUtil.sendEmail(mailMessage);
			System.out.println("Send mail with Attachment");
		} catch (AddressException e) {
			e.printStackTrace();
		}
	}
	
	public void sendTemplatedHTMLEmailWithAttachment(ActionRequest req, ActionResponse resp){
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		
		String to = req.getParameter("to");
		if(to == null || to.trim().equals("")){
			SessionErrors.add(req, "mailEmpty");
		}
		
		String name = req.getParameter("name");
		if(name == null || name.trim().equals("")){
			SessionErrors.add(req, "nameEmpty");
		}
		
		/////////Sacar el contenido de la plantilla guardada en el dir data de la instalaci�n de LF
		String body = "";
		try {
			Scanner sc = new Scanner(plantilla);
			while(sc.hasNext()){
				body += sc.nextLine();
			}
			sc.close();
		} catch (IOException e1) {
			System.out.println(e1.getMessage());
			e1.printStackTrace();
		}
		/*////////////*************Comprobar como pasar el path. Probar en LF 6.2
		String body = ContentUtil.get("content/miplantilla.tmpl", true);
		System.out.println("Plantilla1: " + body);//vacio
		
		body = ContentUtil.get("content\\miplantilla.tmpl");
		System.out.println("Plantilla2: " + body);
		
		body = ContentUtil.get("content.miplantilla.tmpl");
		System.out.println("Plantilla3: " + body);//vacio
		
		body = ContentUtil.get("conte4nt/miplantilla.tmpl");
		System.out.println("Plantilla4: " + body);
		
		PortletContext portletContext = req.getPortletSession().getPortletContext();

		
		String content = ContentUtil.get("templates/miplantilla.tmpl");
		System.out.println("Plantilla 5: " + content);
		
		body = ContentUtil.get("mailTest/templates/plantilla.tmpl");
		System.out.println("Plantilla 6: " + body);*/		
		
		
		body = StringUtil.replace(body, new String[] { "[$NAME$]","[$img$]"}, new String[] { name, "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=2ahUKEwi0rdmb3O3jAhUSkhQKHSwNB3UQjRx6BAgBEAU&url=https%3A%2F%2Fimgcomfort.com%2Fca%2Fabout%2F&psig=AOvVaw113qseUGF43uiJJ8hYjzaH&ust=1565162868501145"});
		
		try {
			fromAddress = new InternetAddress(from);
			toAddress = new InternetAddress(to);
			MailMessage mailMessage = new MailMessage();
			mailMessage.setTo(toAddress);
			mailMessage.setFrom(fromAddress);
			mailMessage.setSubject("Ejemplo con plantilla");
			mailMessage.setBody(body);
			mailMessage.setHTMLFormat(true);
			mailMessage.addFileAttachment(attachment, attachmentName);
			MailServiceUtil.sendEmail(mailMessage);
			System.out.println("Send mail by Using Template");
		} catch (AddressException e) {
			e.printStackTrace();
		}
	}
}