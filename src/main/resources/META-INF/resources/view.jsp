<%@ include file="/init.jsp" %>

<liferay-ui:error key="mailEmpty" message="Introduzca una direcci�n de correo a la que enviar el mensaje" />
<liferay-ui:error key="nameEmpty" message="Introduzca un nombre" />

<portlet:actionURL  var="sendTextEmail" name="sendTextEmail" />
<portlet:actionURL  var="sendTextWithAtEmail" name="sendTextEmailWithAttachment" />
<portlet:actionURL  var="sendHTMLWithAtEmail" name="sendTemplatedHTMLEmailWithAttachment" />

<aui:form action="${sendTextEmail}" method="post" name="textEmail">
	<aui:input name="to" type="email" label="Direcci�n de destino">
		<aui:validator name="required" />
	</aui:input>
	<aui:button type="submit" name="submit" value="Enviar email texto" />
</aui:form>

<hr/>

<aui:form action="${sendTextWithAtEmail}" method="post" name="textEmailWAt">
	<aui:input name="to" type="email" label="Direcci�n de destino">
		<aui:validator name="required" />
	</aui:input>
	<aui:button type="submit" name="submit" value="Enviar email texto con adjunto" />
</aui:form>

<hr/>

<aui:form action="${sendHTMLWithAtEmail}" method="post" name="textEmailWAt">
	<aui:input name="to" type="email" label="Direcci�n de destino">
		<aui:validator name="required" />
	</aui:input>
	<aui:input name="name" type="text" label="Nombre">
		<aui:validator name="required" />
	</aui:input>
	<aui:button type="submit" name="submit" value="Enviar email plantilla HTML con adjunto" />
</aui:form>
